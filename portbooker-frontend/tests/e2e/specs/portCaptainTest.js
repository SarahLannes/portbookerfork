describe('boatOwnerTest', () => {
  it('Visits the app root url', () => {
    cy.visit('/admin')
    cy.contains('Welcome captain!')
  })

  it('From login page to port page', () => {
    cy.visit('/')
    cy.get('#email').type("test@gmail.com")
    cy.get('#password').type("e2eTest1ng")
    cy.get('.blue-button').click()
    cy.contains('Ports near me')
  })

  it('Incoming Bookings are visible', () => {
    cy.visit('/admin/incoming')
    cy.contains('Incoming Bookings')
  })

  it('Log out', () => {
    cy.visit('/admin/port')
    cy.get('#button4').click()
    cy.contains("Sign in to your account")
  })

})
