describe('boatOwnerTest', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('Welcome captain!')
  })

  it('From login page to browse ports page', () => {
    cy.visit('/')
    cy.get('#email').type("test@gmail.com")
    cy.get('#password').type("e2eTest1ng")
    cy.get('.blue-button').click()
    cy.contains('Ports near me')
  })

  it('Opens module from browse ports', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
  })

  it('From browse ports to booking', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
    cy.get('.white-bold-text').click()
    cy.contains('Pick dates to get prices')
  })

  // CHANGEME: change this in future to avoid test bookings in real DB
  it('Successful booking', () => {
    cy.visit('/browse')
    cy.get('.portImage').first().click()
    cy.contains('Address')
    cy.get('.white-bold-text').click()
    cy.contains('Pick dates to get prices')
    cy.get('#startdate').type("2030-10-10")
    cy.get('#enddate').type("2030-11-10")
    cy.get('#submit-button').click()
    cy.get('.false').first().click()
    cy.contains('Booking details')
    cy.contains('Pay with invoice').click()
    cy.get('.payButton').first().click()
    cy.contains('Booking has been successfully made')
  })

  it('My bookings', () => {
    cy.visit('/mybookings')
    cy.contains('Upcoming Bookings')
    cy.contains('Previous Bookings')
  })

  it('Log out', () => {
    cy.visit('/browse')
    cy.get('#button4').click()
    cy.contains("Sign in to your account")
  })
})
