import { shallowMount } from '@vue/test-utils'
import LoginComponent from '../../../portbooker-frontend/src/shared/components/LoginComponent'

/* eslint-env jest */
describe('UserLoginPage', () => {
  it('find welcome message', () => {
    const wrapper = shallowMount(LoginComponent);
    expect(wrapper.find("span").text()).toBe("Welcome captain!")
  })
})
