import {createRouter, createWebHistory} from 'vue-router'
import UserLoginPage from '../BoatOwner/views/UserLoginPage'
import BookAPlace from '../BoatOwner/views/BookAPlace.vue'
import PageNotFound from '../shared/views/PageNotFound.vue'
import BrowsePortsPage from '../BoatOwner/views/BrowsePortsPage.vue'
import SuccessfulBooking from '../BoatOwner/views/SuccessfulBooking.vue'
import SignUpPage from "../BoatOwner/views/SignUpPage.vue"
import firebase from "../../../portbooker-backend/firebase";
import CaptainLoginPage from "../PortCaptain/views/CaptainLoginPage";
import ApplyForAccountPage from "../PortCaptain/views/ApplyForAccountPage";
import MyPort from "../PortCaptain/views/MyPort.vue"
import IncomingBookings from "../PortCaptain/views/IncomingBookings.vue"
import Settings from "../PortCaptain/views/Settings.vue"
require("firebase/auth");
import MyBookings from "../views/MyBookings";


const routes = [
    {
        path: '/',
        name: 'UserLoginPage',
        component: UserLoginPage,
    },
    {
        path: '/admin',
        name: 'CaptainLoginPage',
        component: CaptainLoginPage,
    },
    {
        path: '/register',
        name: 'SignUpPage',
        component: SignUpPage
    },
    {
        path: '/success',
        name: 'SuccessfulBooking',
        component: SuccessfulBooking,
        meta: {requiresAuth: true}
    },
    {
        path: '/mybookings',
        name: 'MyBookings',
        component: MyBookings
    },
    {
        path: '/book',
        name: 'BookAPlace',
        component: BookAPlace,
        meta: {requiresAuth: true}
    },
    {
        path: "/:pathMatch(.*)*",
        name: 'PageNotFound',
        component: PageNotFound
    },
    {
        path: '/admin/port',
        name: 'MyPort',
        component: MyPort
    },
    {
        path: '/admin/settings',
        name: 'Settings',
        component: Settings
    },
    {
        path: '/admin/register',
        name: 'ApplyForAccountPage',
        component: ApplyForAccountPage
    },
    {
        path: '/browse',
        name: 'BrowsePortsPage',
        component: BrowsePortsPage,
        meta: {requiresAuth: true}
    },
    {
        path: '/admin/incoming',
        name: 'IncomingBookings',
        component: IncomingBookings
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})


router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const isAuthenticated = firebase.auth().currentUser;
    if (requiresAuth && !isAuthenticated) {
        next("/");
    } else {
        next();
    }

})

export default router
