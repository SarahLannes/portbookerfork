import {createApp} from 'vue'
import App from './App.vue'
import axios from 'axios';
import firebase from "../../portbooker-backend/firebase";

require("firebase/auth");

import router from './router'
import Vuex from 'vuex'


let app;

const store = new Vuex.Store({
    state: {
        port: ""
    }, mutations: {
        change(state, portName) {
            state.port = portName;
        }
    }
});

firebase.auth().onAuthStateChanged(() => {
    if (!app) {

        app = createApp(App);
        app.use(router);
        app.use(store);
        app.mount('#app');
        app.provide('$axios', axios);
    }
})
