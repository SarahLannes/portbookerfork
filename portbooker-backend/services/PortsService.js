import firebase from '../firebase';
import {doc, getDoc} from 'firebase/firestore';

// Get all of the ports

const firestore = firebase.firestore();

export class PortsService {
  constructor(address, alt, coordinates, email, mobile, portName, picture, radio, webpage) {
    this.address = address
    this.alt = alt
    this.coordinates = coordinates
    this.email = email
    this.mobile = mobile
    this.portName = portName
    this.picture = picture
    this.radio = radio
    this.webpage = webpage
  }

  getDataObjectForPort() {
    return {
      address: this.address,
      alt: this.alt,
      coordinates: this.coordinates,
      email: this.email,
      mobile: this.mobile,
      name: this.portName,
      picture: this.picture,
      radio: this.radio,
      webpage: this.webpage,
    }
  }

  getDataObjectForApplication() {
    return {
      address: this.address,
      alt: this.alt,
      coordinates: this.coordinates,
      email: this.email,
      mobile: this.mobile,
      name: this.portName,
      radio: this.radio,
      webpage: this.webpage,
    }
  }
  // Add new port
// eslint-disable-next-line max-len
   async addPort () {
    await firestore.collection('ports').doc(this.portName).set(this.getDataObjectForPort());
  };

  // Add new port
// eslint-disable-next-line max-len
  async addPortApplication () {
    await firestore.collection('portApplications').doc(this.portName).set(this.getDataObjectForApplication());
  };

};

export const getAllPorts = async () => {
  const ports = [];

  await firestore.collection('ports').get().then((querySnapshot) => {
    querySnapshot.docs.forEach((doc) => {
      ports.push(doc.data());
    });
  });

  return ports;
};

// Get the port documents by name
export const getPortByName = async (portName) => {
  const docRef = doc(firestore, 'ports', portName);
  const docSnap = await getDoc(docRef);

  if (docSnap) {
    return docSnap.data();
  } else {
    return null;
  }
};




