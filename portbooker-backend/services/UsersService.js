import firebase from '../firebase';
import {doc, getDoc} from 'firebase/firestore';

const firestore = firebase.firestore();

// Add a new user
export const addUser = async (userID, fullName, phoneNumber, boatNumber) => {
    await firestore.collection('users').doc(userID).set({
        fullName: fullName,
        phoneNumber: phoneNumber,
        boatNumber: boatNumber,
    });
};

// Get user info
export const getUser = async (userID) => {
    const user= await firestore.collection('users').doc(userID).get();
    if (user) {
        return user.data();
    } else {
        return null;
    }
};
