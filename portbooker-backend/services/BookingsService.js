import firebase from '../firebase';
import {getAllPorts, getPortByName} from "./PortsService";
import {doc, getDoc} from "firebase/firestore";

const firestore = firebase.firestore();

export class BookingsService {
    constructor(userId, placeId, startTime, endTime, price, passengers, port) {
        this.userId = userId
        this.placeId = placeId
        this.startTime = startTime
        this.endTime = endTime
        this.price = price
        this.passengers = passengers
        this.port = port
    }

    async getBookingDataObject() {
        const userRef = firestore.collection('users').doc(this.userId);

        return {
            userId: userRef,
            placeId: this.placeId,
            startTime: this.startTime,
            endTime: this.endTime,
            price: this.price,
            passengers: this.passengers,
            port: this.port
        }
    }


    async createBooking(portName) {
        const bookingsRef = firestore.collection('ports').doc(portName)
            .collection('bookings');
        const bookingData = await this.getBookingDataObject();
        await bookingsRef.doc().set(bookingData);
    }

}

export const getAllUserBookings = async (uid) => {
    const ports = await firebase
        .firestore()
        .collection("ports")
        .get();
    const promises = [];
    const previousBookings = [];
    const upcomingBookings = [];
    ports.forEach(snapshot => {
        promises.push(snapshot.ref.collection('bookings').get());
    });
    const bookings = await Promise.all(promises);
    const currentDate = new Date();
    bookings.forEach(snapshotArray => {
        snapshotArray.forEach(doc => {
            if (doc.data().userId.id.toString() === uid.toString()) {
                const date = doc.data().endTime
                if (new Date(date) > currentDate) {
                    upcomingBookings.push(doc.data())
                } else {
                    previousBookings.push(doc.data())
                }
            }
        })
    });

    return [upcomingBookings, previousBookings]
}



