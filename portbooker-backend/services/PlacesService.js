import firebase from '../firebase';

const firestore = firebase.firestore();

export class PlacesService {
    constructor(depth, length, price, width, booked) {
        this.depth = depth
        this.length = length
        this.price = price
        this.width = width
        this.booked = booked
    }

    getDataObject() {
        return {
            depth: this.depth,
            length: this.length,
            price: this.price,
            width: this.width,
            booked: this.booked,
        }
    }

    async addToPort(portName) {
        const placeRef = firestore.collection('ports').doc(portName)
            .collection('places');

        const placesCount = await firestore.collection('ports').doc(portName)
            .collection('places').get().then((snap) => {
                return snap.size;
            });
        await placeRef.doc(String(placesCount + 1)).set(this.getDataObject());
    }
}

// Get place from port by ID
export const getPlace = async (portName, placeId) => {
    const placeRef = await firestore.collection('ports').doc(portName)
        .collection('places').doc(placeId).get();

    if (placeRef) {
        return placeRef.data();
    } else {
        return null;
    }
};

export const getAllPlaces = async (portName) => {
    const places = [];

    await firestore.collection('ports').doc(portName)
        .collection('places').get().then((querySnapshot) => {
            querySnapshot.docs.forEach((doc) => {
                places.push({id: doc.id, ...doc.data()});
            })
        });

    return places;
};

// Edit place
export const editPlace = async (portName, placeId, newData) => {
    const placeRef = firestore.collection('ports').doc(portName)
        .collection('places');
    await placeRef.doc(placeId).update({...newData});
};
