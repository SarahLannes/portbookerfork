import firebase from '../firebase';

// Get all of the ports

const firestore = firebase.firestore();


// Add a new boat
// eslint-disable-next-line max-len
export const addBoat = async (registryNumber, boatName, boatModel, width, length, depth) => {
    await firestore.collection('boats').doc(registryNumber).set({
        boatName: boatName,
        boatModel: boatModel,
        width: width,
        length: length,
        depth: depth,
    });
};
