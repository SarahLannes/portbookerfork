// Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyA0bVeORlJBrPAljjAK_jS3d2pwYZzcDyw',
  authDomain: 'portbooker-388bf.firebaseapp.com',
  projectId: 'portbooker-388bf',
  storageBucket: 'portbooker-388bf.appspot.com',
  messagingSenderId: '144554639316',
  appId: '1:144554639316:web:261fce158c6542c06ae041',
  measurementId: 'G-1D2YVBHWVB',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;

